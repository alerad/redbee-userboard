package models

import java.util.Date

import play.api.libs.json._
import play.api.libs.functional.syntax._
import reactivemongo.bson.{BSONDocumentHandler, BSONObjectID}


case class Board (
   _id: String = BSONObjectID.generate().toString(),
   user : String,
   interests: Seq[String],
   cached: Boolean=false,
   lastUpdated: Date=new Date(),
   title: String = "NewBoard"

 )

//Para JSON serialization
object Board {
  implicit val boardFormatJson : OFormat[Board] = Json.using[Json.WithDefaultValues].format[Board]

  import reactivemongo.bson.Macros

  implicit val boardFormat : BSONDocumentHandler[Board] = Macros.handler[Board]
}



