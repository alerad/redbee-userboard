package models

import play.api.libs.json.{Json, OFormat}
import reactivemongo.bson.{BSONDocumentHandler, BSONObjectID}

case class User(
                 _id: String = BSONObjectID.generate().toString(),
                 userName: String,
                 password: String
               )


//Para BSON serialization cuando upsertea
object User {
  import reactivemongo.bson.Macros
  implicit val userFormatJson : OFormat[User] = Json.format[User]
  implicit val userFormat : BSONDocumentHandler[User] = Macros.handler[User]

}


