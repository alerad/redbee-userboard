package services

import javax.inject.Inject

import api.JwtHelper
import models.{Board, User}
import play.api.libs.json._
import play.api.mvc.{AnyContent, RequestHeader}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{BSON, BSONDocument, BSONObjectID}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}

class BoardsService @Inject()
(val reactiveMongoApi: ReactiveMongoApi, jwtHelper: JwtHelper) {

  def boardCollection = reactiveMongoApi.database.map(_.collection[BSONCollection]("boards"))


  def getBoardFromBody(body: AnyContent, user: User) : Board = {
    val jsonBody = Json.parse(body.asJson.get.toString()).as[JsObject]
    (jsonBody + ("user" -> Json.toJson(user._id))).as[Board] //Agrego user

  }

  def insert(body: AnyContent,request: RequestHeader): Future[Board] = {
    val promise: Promise[Board] = Promise[Board]()

    val header = request.headers.get("Authorization").get
    val tokenUser = jwtHelper.getUserFromToken(header)
    val board = getBoardFromBody(body, tokenUser)

    boardCollection.flatMap(_.insert(board).map(_ => {}))
    promise.success(board)
    promise.future
  }

  def findUserBoards(request: RequestHeader) : Future[List[BSONDocument]] = {
    val header = request.headers.get("Authorization").get
    val tokenUser = jwtHelper.getUserFromToken(header)
    val query = BSONDocument("user" -> tokenUser._id)
    val boards = findByQuery(query)
    boards
  }

  def update(body: AnyContent, request: RequestHeader, id: String) : Future[Option[Board]] = {
    val header = request.headers.get("Authorization").get
    val tokenUser = jwtHelper.getUserFromToken(header)

    updateBoard(body, tokenUser, id)
  }

  def updateBoard(body: AnyContent, user: User, id: String): Future[Option[Board]]= {
    val board = getBoardFromBody(body, user)
    val boardBson = BSON.writeDocument[Board](board)
    var selector = BSONDocument("_id" ->  BSONObjectID.parse(id).get.toString())

    boardCollection.flatMap(_.findAndUpdate(selector, boardBson.remove("_id"), fetchNewObject = true).map(_.result[Board]))
  }


  def findOne(id : String, request: RequestHeader) : Future[Option[Board]] = {
    val query = BSONDocument("_id" -> BSONObjectID.parse(id).get.toString())
    boardCollection.flatMap(_.find(query).one[Board])
  }


  def findByQuery(query: BSONDocument) : Future[List[BSONDocument]]= {
    val boards : Future[List[BSONDocument]] = boardCollection.flatMap(_.find(query).cursor().collect[List]())
    boards
  }



}
