package services

import javax.inject.Inject

import models.User
import play.api.libs.json.JsValue
import play.api.mvc.AnyContent
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSON, BSONDocument}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}


class UsersService @Inject()
(val reactiveMongoApi: ReactiveMongoApi) {


  def userCollection = reactiveMongoApi.database.map(_.collection[BSONCollection]("users"))


  def getUserFromBody(body: AnyContent) : User = {
    val jsonBody: Option[JsValue] = body.asJson

    val userName = jsonBody.get("userName").as[String]
    val password = jsonBody.get("password").as[String]

    def user = new User(userName = userName, password=password)
    user
  }

  def findOne(userName : String) : Future[Option[User]] = {
    val query = BSONDocument("userName" -> userName)
    findByQuery(query)
  }


  def findByUserAndPassword(user: User) : Future[Option[User]]= {
    val query = BSONDocument(
      "userName" -> user.userName,
      "password" -> user.password
    )
    findByQuery(query)
  }

  def updateUser(body: AnyContent, userName: String) : Future[User] = {
    val promise: Promise[User] = Promise[User]()
    var selector = BSONDocument("userName" -> userName)
    promise.success(update(selector, body)).future
  }

  def update(selector: BSONDocument, body: AnyContent): User = {
    val user = getUserFromBody(body)
    val bsonUser = BSON.writeDocument[User](user)
    userCollection.flatMap(_.update(selector, bsonUser.remove("_id")))
    user
  }

  def insertUser(body: AnyContent): Future[User] = {
    val promise: Promise[User] = Promise[User]()
    val user = getUserFromBody(body)
    userCollection.flatMap(_.insert(user).map(_ => {}))
    promise.success(user)
    promise.future
  }


  def findByQuery(query: BSONDocument) : Future[Option[User]]= {
    val foundUser : Future[Option[User]] = userCollection.flatMap(_.find(query).cursor[User]().headOption)
    foundUser
  }

  def delete(userName :String): Future[WriteResult] = {
    val promise: Promise[User] = Promise[User]()
    userCollection.flatMap(_.remove(BSONDocument("userName" -> userName)))
  }
}
