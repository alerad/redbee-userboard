package api.responses

import play.api.libs.json.Json

case class ErrorResponse (
                         developerMessage : String,
                         userMessage: String = ""
                         )


//Para JSON serialization
object ErrorResponse {
  implicit val errorResponseFormatJson = Json.format[ErrorResponse]
}
