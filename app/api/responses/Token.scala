package api.responses

import models.User
import play.api.libs.json.{Json, OFormat}

case class Token (token: String,
                  user: User)


//Para JSON serialization
object Token {
  implicit val boardFormatJson : OFormat[Token] = Json.format[Token]
}
