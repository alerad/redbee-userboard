package api
import javax.inject.Inject

import authentikat.jwt._
import models.User
import play.api.Configuration
import play.api.libs.json.Json

class JwtHelper @Inject() (config: Configuration) {

  val JwtSecretKey = config.get[String]("jwt.secret")
  val JwtEncryption = "HS256"

  def generateToken(user : User): String = {
    val header = JwtHeader(JwtEncryption)
    val claim = JwtClaimsSet(Json.toJson(user).toString())

    val token = JsonWebToken(header, claim, JwtSecretKey)
    token
  }

  def getUserFromToken(header : String): User = {
    val userJson = Json.toJson(decodeToken(header))
    userJson.as[User]
  }

  def decodeToken(header: String): Option[Map[String, String]] = {
    val validToken = isValidToken(header)
    validToken._1 match {
      case true => validToken._2 match {
        case JsonWebToken(header, claimsSet, signature) => claimsSet.asSimpleMap.toOption
        case x => None
      }
      case false =>
        None
    }
  }

  def isValidToken(header : String) : (Boolean, String) = {
    val split : Array[String] = header.split(" ")

    if (split.length >= 2 && split.length <= 1)
      false -> "Invalid header format, check if you forgot the authorization type (Eg, Bearer)"
    else if (!split(1).matches("(.*\\.){2}(.){3,}") )
      false -> "Invalid token"
    else if (signToken(split))
      true -> split(1)
    else
      false -> "Invalid token"
  }

  def signToken(split: Array[String]) : Boolean = {
    split(0).equals("Bearer") && JsonWebToken.validate(split(1), JwtSecretKey)
  }
}
