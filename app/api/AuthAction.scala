package api

import javax.inject.Inject

import akka.http.javadsl.model.StatusCodes
import akka.util.ByteString
import api.responses.ErrorResponse
import play.api.http.HttpEntity
import play.api.libs.json.Json

import scala.concurrent.Future
import play.api.mvc._

case class AuthAction[A]@Inject()(jwtHelper: JwtHelper, userName: Option[String] = None)(action: Action[A]) extends Action[A] {


  def apply(request: Request[A]): Future[Result] = {
     request.headers.get("Authorization") match {
       case None => Future.successful(badRequest("No Authorization Header sent", StatusCodes.BAD_REQUEST.intValue()))
       case Some(header) => {
         val tokenSign = jwtHelper.isValidToken(header)

         if (tokenSign._1){
           userName match {
             case None => action(request)
             case Some(user) => {
               val userBson = jwtHelper.decodeToken("Bearer " + tokenSign._2) //TODO, cambiar el metodo decode para que acepte ambas/refactor
               if (!userBson.get("userName").equals(user))
                 Future.successful(badRequest(errorMessage = "You cant edit others users data.", StatusCodes.UNAUTHORIZED.intValue()))
               else
                 action(request)
             }
           }
         }

         else
           Future.successful(badRequest(tokenSign._2, StatusCodes.BAD_REQUEST.intValue()))
       }
     }
  }


  def badRequest(errorMessage: String, statusCode : Integer) : Result = {
    val unauthByteString = ByteString(Json.toJson(new ErrorResponse(developerMessage = errorMessage)).toString())

    Result(
      header = ResponseHeader(400, Map.empty),
      body = HttpEntity.Strict(unauthByteString, Some("application/json"))
    )
  }



  override def parser = action.parser
  override def executionContext = action.executionContext
}
