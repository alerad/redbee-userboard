package controllers

import javax.inject.{Inject, Singleton}

import api.responses.ErrorResponse
import api.{AuthAction, JwtHelper}
import models.Board
import play.api.libs.json.{JsArray, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import services.BoardsService

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class BoardsController @Inject()(cc : ControllerComponents, boardsService: BoardsService, jwtHelper: JwtHelper) extends AbstractController(cc) {

  def get(id: String) = AuthAction(jwtHelper) {
    Action.async { request =>
      boardsService.findOne(id, request).map {
        case None => BadRequest(Json.toJson(new ErrorResponse(developerMessage = "Board doesn't exist :'(")))
        case Some(board) =>  Ok(Json.toJson(board))
      }
    }
  }
//
  def getAll = Action.async { request =>
    boardsService.findUserBoards(request).map { boards =>
      Ok(JsArray(boards.map { board =>
        //Esto porque no tengo idea como editar los writes de un json, asique hago que ejecute los reads... triste realidad.
        Json.toJson(Json.toJson(board.as[Board]).as[Board])
      }))
    }
  }

  def post = Action.async { request =>
    boardsService.insert(request.body, request).map { board => Ok(Json.toJson(board)) }
  }

  def put(id : String) = Action.async { request =>
    boardsService.update(request.body, request, id).map {
      case None => BadRequest(Json.toJson(new ErrorResponse(developerMessage = "Error updating board")))
      case Some(board) =>  Ok(Json.toJson(board))
    }
  }

  def delete(id : String) = Action {
    Ok("Deletea2")
  }


}
