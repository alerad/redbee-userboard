package controllers

import javax.inject.{Inject, Singleton}

import api.responses.{ErrorResponse, Token}
import api.{AuthAction, JwtHelper}
import play.api.libs.json.Json
import play.api.mvc._
import services.UsersService

import scala.concurrent.ExecutionContext.Implicits.global


@Singleton
class UsersController @Inject()
(cc: ControllerComponents, usersService: UsersService, jwtHelper: JwtHelper) extends AbstractController(cc) {

  def get(userName: String) = AuthAction(jwtHelper, Some(userName)) {
    Action.async { request =>
      usersService.findOne(userName).map {
        case None => BadRequest(Json.toJson(new ErrorResponse(developerMessage = "Incorrect username")))
        case Some(user) =>  Ok(Json.toJson(user))
      }
    }
  }

  def authenticate = Action.async { request =>
    def user = usersService.getUserFromBody(request.body)
    def foundUser = usersService.findByUserAndPassword(user)
    foundUser.map {
      case None => BadRequest(Json.toJson(new ErrorResponse(developerMessage = "Incorrect username or password")))
      case Some(user) =>  Ok(Json.toJson(new Token(token=jwtHelper.generateToken(user), user=user)))
    }
  }



  def post = Action.async { request =>
    usersService.insertUser(request.body).map { user => Ok(Json.toJson(user)) }
  }

  def put(userName : String) = AuthAction(jwtHelper, Some(userName)) {
    Action.async { request =>
      usersService.updateUser(request.body,userName).map { res =>
        Ok(Json.toJson(res))
      }
    }
  }

  def delete(userName : String) = AuthAction(jwtHelper, Some(userName)) {
    Action.async { request=>
      usersService.delete(userName).map { res =>
        if (res.n > 0)
          Ok(s"User was deleted")
        else
          NotModified
      }
    }
  }




}
